import { useReactMediaRecorder } from "react-media-recorder";
import { createFileName } from "use-react-screenshot";


const Record = () => {
    const { status, startRecording, stopRecording, mediaBlobUrl } = useReactMediaRecorder({ screen: true });
    const download = (video, { name = "Client Side Recording", extension = "mp4" } = {}) => {
        const a = document.createElement("a");
        a.href = mediaBlobUrl;
        a.download = createFileName(extension, name);
        a.click();
    }

    return (
        <div>
            <p>{status}</p>
            <button className="btn startRecord" onClick={startRecording}>Start Recording</button>
            <button className="btn stopRecord" onClick={stopRecording}>Stop Recording</button>
            <div>{mediaBlobUrl && (<button className="btn" onClick={download}>Download Video Record</button>)}</div>
        </div>
    );
};

export default Record;
