/// <reference types="cypress"/>

describe('Basic Test',()=> {
    it('Should load the website without error',()=>{
        cy.visit('https://codedamn.com')
    })

    it.only('Login page looks good',()=>{
        cy.viewport(1280,720)
        cy.visit('https://codedamn.com')
        cy.contains('Sign in').click()
        cy.contains('Sign in to codedamn').should('exist')
        cy.contains('Forgot your password?').should('exist')
        cy.contains('Remember me').should('exist')
    })
})